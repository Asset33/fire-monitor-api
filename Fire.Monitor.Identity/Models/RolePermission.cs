﻿using Microsoft.AspNetCore.Identity;

namespace Fire.Monitor.Identity.Models;

public class RolePermission
{
    public Guid Id { get; set; }
    public Guid PermissionId { get; set; }
    public Permission Permission { get; set; }
    public Guid RoleId { get; set; }
    public IdentityRole Role { get; set; }
}