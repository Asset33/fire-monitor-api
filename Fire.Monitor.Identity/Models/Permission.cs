﻿using Fire.Monitor.Domain.Primitives;

namespace Fire.Monitor.Identity.Models;

public class Permission
{
    public Guid Id { get; set; }
    public PermissionType Name { get; set; }
    public string Description { get; set; }
    public ICollection<RolePermission> RolePermissions { get; set; }
}