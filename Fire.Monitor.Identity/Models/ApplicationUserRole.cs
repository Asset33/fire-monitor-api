﻿using Microsoft.AspNetCore.Identity;

namespace Fire.Monitor.Identity.Models;

public class ApplicationUserRole: IdentityUserRole<Guid>
{
    public virtual ApplicationUser User { get; set; }
        
    public virtual Role Role { get; set; }
    
}