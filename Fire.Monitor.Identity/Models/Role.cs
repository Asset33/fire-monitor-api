﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace Fire.Monitor.Identity.Models;

public class Role: IdentityRole<Guid>
{
    public Role(string roleName)
    {
        base.Name = roleName;
        Discriminator = "IdentityRole";
    }
        
    public Role()
    {
            
    }
    [Required]
    public string Discriminator { get; set; }

    public string RuName { get; set; }
    public ICollection<ApplicationUserRole> UserRoles { get; set; }
    public virtual ICollection<RolePermission> RolePermissions { get; set; } 
    
    
}