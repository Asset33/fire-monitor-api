﻿using Microsoft.AspNetCore.Identity;

namespace Fire.Monitor.Identity.Models;

public class ApplicationUser : IdentityUser<Guid>
{
    public string FirstName { get; set; }
    public string LastName { get; set; }

    public string Iin { get; set; }
    public ICollection<ApplicationUserRole> UserRoles { get; set; }
    public List<RefreshToken> RefreshTokens { get; set; }
    public bool IsDelete { get; set; }
    public bool IsAccessToSystem { get; set; } = true;
    public bool OwnsToken(string token)
    {
        return this.RefreshTokens?.Find(x => x.Token == token) != null;
    }
}