﻿using Fire.Monitor.Domain.Entities.FireMonitor;
using Fire.Monitor.Domain.Entities.WeatherForecast;
using Microsoft.EntityFrameworkCore;

namespace Fire.Monitor.Application.Interfaces;

public interface IApplicationDbContext
{
    public DbSet<FireMonitorData> FireMonitorDatas { get; set; }
    public DbSet<Detection> Detections { get; set; }
    public DbSet<Weather> Weathers { get; set; }
    Task<int> SaveChangesAsync(CancellationToken cancellationToken);
}