﻿using AutoMapper;
using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Domain.Entities.FireMonitor;
using Fire.Monitor.Domain.Entities.WeatherForecast;

namespace Fire.Monitor.Application.Mapping
{
    public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            MappingFireMonitorData();
            MappingDetection();
            MappingWeather();
        }

        private void MappingFireMonitorData()
        {
            CreateMap<FireMonitorDataDto, FireMonitorData>()
                .ReverseMap();
            
        }

        private void MappingDetection()
        {
            CreateMap<Detection, DetectionsDto>();
            CreateMap<DetectionsDto, Detection>();
        }

        private void MappingWeather()
        {
            CreateMap<Weather, WeatherDto>().ReverseMap();
            CreateMap<Location, LocationDto>().ReverseMap();
            CreateMap<Wind, WindDto>().ReverseMap();
            CreateMap<SystemInfo, SystemInfoDto>().ReverseMap();
            CreateMap<Temperature, TemperatureDto>().ReverseMap();
        }
    }
}