﻿using AutoMapper;
using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Interfaces;
using Fire.Monitor.Application.Responses;
using Fire.Monitor.Domain.Entities.FireMonitor;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Fire.Monitor.Application.Logics.FireMonitorLogic.Command.Impl;

public class FireMonitorCommandLogic : IFireMonitorCommandLogic
{
    private readonly IApplicationDbContext _context;
    private readonly IConfiguration _config;
    private readonly IMapper _mapper;

    public FireMonitorCommandLogic(IApplicationDbContext context, IConfiguration config, IMapper mapper)
    {
        _context = context;
        _config = config;
        _mapper = mapper;
    }

    public async Task<Response<int>> Add(FireMonitorDataDto dto)
    {
        var fm = new FireMonitorData()
        { 
            Detections = _mapper.Map<List<Detection>>(dto.Detections),
            Created = DateTime.Now,
            IsDelete = false
        };

        await _context.FireMonitorDatas.AddAsync(fm);
        var result = await _context.SaveChangesAsync(CancellationToken.None);
        return new Response<int>(result);
    }

    public Task<Response<int>> Update(Guid Id)
    {
        throw new NotImplementedException();
    }

    public async Task<Response<bool>> Delete(Guid Id)
    {
        bool result = false;
        var fm = await _context.FireMonitorDatas
            .Where(d => d.IsDelete == false && Id == d.Id)
            .AsNoTracking()
            .FirstOrDefaultAsync();
        if (fm != null)
        {
            fm.IsDelete = true;
            _context.FireMonitorDatas.Update(fm);
            result = Convert.ToBoolean(await _context.SaveChangesAsync(CancellationToken.None));
        }
        else result = false;
        
        return new Response<bool>(result);
    }
}