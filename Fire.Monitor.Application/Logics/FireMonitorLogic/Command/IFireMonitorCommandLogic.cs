﻿using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Responses;

namespace Fire.Monitor.Application.Logics.FireMonitorLogic.Command;

public interface IFireMonitorCommandLogic
{
    Task<Response<int>> Add(FireMonitorDataDto dto);
    Task<Response<int>> Update(Guid Id);
    Task<Response<bool>> Delete(Guid Id);
}