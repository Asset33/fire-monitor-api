﻿using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Dtos.FireMonitor.Request;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Responses;

namespace Fire.Monitor.Application.Logics.FireMonitorLogic.Query;

public interface IFireMonitorQueryLogic
{
    Task<PagedResponse<ICollection<FireMonitorDataDto>>> GetAll(GetAllFireMonitorQuery request);
    Task<PagedResponse<ICollection<FireMonitorDataDto>>> GetFiltered(GetFilteredFireMonitorQuery query);
}