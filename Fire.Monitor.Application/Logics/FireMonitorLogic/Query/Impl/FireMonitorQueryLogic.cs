﻿using System.Linq;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Dtos.FireMonitor.Request;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Interfaces;
using Fire.Monitor.Application.Responses;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Fire.Monitor.Application.Logics.FireMonitorLogic.Query.Impl;

public class FireMonitorQueryLogic : IFireMonitorQueryLogic
{
    private readonly IApplicationDbContext _context;
    private readonly IConfiguration _config;
    private readonly IMapper _mapper;

    public FireMonitorQueryLogic(IApplicationDbContext context, IConfiguration config, IMapper mapper)
    {
        _context = context;
        _config = config;
        _mapper = mapper;
    }

    public async Task<PagedResponse<ICollection<FireMonitorDataDto>>> GetAll(GetAllFireMonitorQuery request)
    {
        var count = await _context.FireMonitorDatas
            .Where(x => x.IsDelete == false)
            .Include(x=>x.Detections)
            .CountAsync();
        
        var result = await _context.FireMonitorDatas
            .Where(x => x.IsDelete == false)
            .ProjectTo<FireMonitorDataDto>(_mapper.ConfigurationProvider)
            .Skip((request.PageNumber - 1) * request.PageSize)
            .Take(request.PageSize)
            .ToListAsync();

        return new PagedResponse<ICollection<FireMonitorDataDto>>(result, count);
    }

    public async Task<PagedResponse<ICollection<FireMonitorDataDto>>> GetFiltered(GetFilteredFireMonitorQuery request)
    {
        var count = await _context.FireMonitorDatas
            .Where(x => x.IsDelete == false)
            .Where(x => request.Id == null || x.Id == request.Id)
            .Where(x => request.Date == null || x.Created.Date == request.Date)
            .Include(x=>x.Detections)
            .Where(x=> string.IsNullOrWhiteSpace(request.Prob) || x.Detections.Any(d=>d.Prob.ToString().Contains(request.Prob)))
            .CountAsync();
        
        var result = await _context.FireMonitorDatas
            .Where(x => x.IsDelete == false)
            .Where(x => request.Id == null || x.Id == request.Id)
            .Where(x => request.Date == null || x.Created.Date == request.Date)
            .Include(x=>x.Detections)
            .Where(x=> string.IsNullOrWhiteSpace(request.Prob) || x.Detections.Any(d=>d.Prob.ToString().Contains(request.Prob)))
            .ProjectTo<FireMonitorDataDto>(_mapper.ConfigurationProvider)
            .Skip((request.PageNumber - 1) * request.PageSize)
            .Take(request.PageSize)
            .ToListAsync();

        return new PagedResponse<ICollection<FireMonitorDataDto>>(result, count);
    }
}