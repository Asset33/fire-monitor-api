﻿using System.Net;
using AutoMapper;
using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Exceptions;
using Fire.Monitor.Application.Helpers;
using Fire.Monitor.Application.Interfaces;
using Fire.Monitor.Application.Responses;
using Fire.Monitor.Domain.Entities.WeatherForecast;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Fire.Monitor.Application.Logics.WeatherForecastLogic.Command.Impl;

public class ForecastCommandLogic : IForecastCommandLogic
{
    private readonly IApplicationDbContext _context;
    private readonly IConfiguration _config;
    private readonly IMapper _mapper;

    public ForecastCommandLogic(IApplicationDbContext context, IConfiguration config, IMapper mapper)
    {
        _context = context;
        _config = config;
        _mapper = mapper;
    }

    public async Task<WeatherDto?> GetAndSaveCurrentForecastByLocation(GetForecastByLocationQuery request)
    {
        var key = _config.GetValue<string>("WeatherForecast:Appid");
        var weatherForecast = await HelperWeather.GetWeatherForecastAsync(request.Latitude, request.Longitude, key);
        var weather = _mapper.Map<Weather>(weatherForecast);
        weather.Created = DateTime.Now;
        await _context.Weathers.AddAsync(weather);
        await _context.SaveChangesAsync(CancellationToken.None);
        return weatherForecast;
    }

    public async Task<Response<int>> Delete(GetByIdQuery request)
    {
        var weather = _context.Weathers.FirstOrDefault(w=>w.Id == request.Id);
        if (weather == null)
        {
            throw new HttpException(HttpStatusCode.NotFound, "Not Found");
        }

        weather.IsDelete = true;
        weather.UpdateDate = DateTime.Now;

        var result = await _context.SaveChangesAsync(CancellationToken.None);
        return new Response<int>(result);
    }
}