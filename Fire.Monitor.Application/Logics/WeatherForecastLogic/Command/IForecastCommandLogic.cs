﻿using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Responses;

namespace Fire.Monitor.Application.Logics.WeatherForecastLogic.Command;

public interface IForecastCommandLogic
{
    Task<WeatherDto?> GetAndSaveCurrentForecastByLocation(GetForecastByLocationQuery request);
    Task<Response<int>> Delete(GetByIdQuery request);
}