﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Helpers;
using Fire.Monitor.Application.Interfaces;
using Fire.Monitor.Application.Responses;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Fire.Monitor.Application.Logics.WeatherForecastLogic.Query.Impl;

public class ForecastQueryLogic : IForecastQueryLogic
{
    private readonly IApplicationDbContext _context;
    private readonly IConfiguration _config;
    private readonly IMapper _mapper;

    public ForecastQueryLogic(IApplicationDbContext context, IConfiguration config, IMapper mapper)
    {
        _context = context;
        _config = config;
        _mapper = mapper;
    }

    public async Task<WeatherDto?> GetForecastByLocation(GetForecastByLocationQuery request)
    {
        var key = _config.GetValue<string>("WeatherForecast:Appid");
        var weatherForecast = await HelperWeather.GetWeatherForecastAsync(request.Latitude, request.Longitude, key);
        return weatherForecast;
    }

    public async Task<PagedResponse<ICollection<WeatherDto>>> GetFilteredForecastHistory(GetFilteredForecastHistoryQuery request)
    {
        var count = await _context.Weathers
            .Where(w => w.IsDelete == false)
            .Where(w => request.Id == null || w.Id == request.Id)
            .Where(w => string.IsNullOrWhiteSpace(request.Latitude) || w.Coord.Lat.ToString().Contains(request.Latitude))
            .Where(w => string.IsNullOrWhiteSpace(request.Longitude) || w.Coord.Lon.ToString().Contains(request.Longitude))
            .Where(w => request.Date == null || w.Created.Date == request.Date)
            .CountAsync();
        
        var result = await _context.Weathers
            .Where(w => w.IsDelete == false)
            .Where(w => request.Id == null || w.Id == request.Id)
            .Where(w => string.IsNullOrWhiteSpace(request.Latitude) || w.Coord.Lat.ToString().Contains(request.Latitude))
            .Where(w => string.IsNullOrWhiteSpace(request.Longitude) || w.Coord.Lon.ToString().Contains(request.Longitude))
            .Where(w => request.Date == null || w.Created.Date == request.Date)
            .ProjectTo<WeatherDto>(_mapper.ConfigurationProvider)
            .Skip((request.PageNumber - 1) * request.PageSize)
            .Take(request.PageSize)
            .ToListAsync();
        
        return new PagedResponse<ICollection<WeatherDto>>(result, count);
    }
    
    
}