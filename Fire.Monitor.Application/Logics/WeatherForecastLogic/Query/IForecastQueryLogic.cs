﻿using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Responses;

namespace Fire.Monitor.Application.Logics.WeatherForecastLogic.Query;

public interface IForecastQueryLogic
{
    Task<WeatherDto?> GetForecastByLocation(GetForecastByLocationQuery request);

    Task<PagedResponse<ICollection<WeatherDto>>> GetFilteredForecastHistory(GetFilteredForecastHistoryQuery request);
}