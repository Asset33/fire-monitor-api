﻿
namespace Fire.Monitor.Application.Responses
{
    public class PagedResponse<T> : Response<T>
    {
        public int TotalRecords { get; set; }

        public PagedResponse(T data, int totalRecords)
        {
            Data = data;
            TotalRecords = totalRecords;
            Succeeded = true;
            Message = null;
            Error = null;
        }
    }
}