﻿using System.Text.Json.Serialization;

namespace Fire.Monitor.Application.Dtos.FireMonitor;

public class DetectionsDto
{
    [JsonPropertyName("prob")]
    public decimal Prob { get; set; }
    
    [JsonPropertyName("up_lat")]
    public decimal UpLat { get; set; }

    [JsonPropertyName("left_lon")]
    public decimal LeftLon { get; set; }
        
    [JsonPropertyName("down_lat")]
    public decimal Downlat { get; set; }
        
    [JsonPropertyName("right_lon")]
    public decimal RightLon { get; set; }
}