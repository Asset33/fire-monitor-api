﻿namespace Fire.Monitor.Application.Dtos.FireMonitor;

public class FireMonitorDataDto
{
    public ICollection<DetectionsDto>? Detections { get; set; }
    public DateTime Created { get; set; }
}