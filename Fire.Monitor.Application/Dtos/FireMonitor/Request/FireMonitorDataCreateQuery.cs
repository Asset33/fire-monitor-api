﻿namespace Fire.Monitor.Application.Dtos.FireMonitor.Request;

public class FireMonitorDataCreateQuery
{
    /// <summary>
    /// Данные о пожарах
    /// </summary>
    public ICollection<DetectionsDto>? Detections { get; set; }
    public DateTime Created { get; set; }
}