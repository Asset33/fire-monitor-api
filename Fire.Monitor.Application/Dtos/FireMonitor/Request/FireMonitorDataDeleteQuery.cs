﻿namespace Fire.Monitor.Application.Dtos.FireMonitor.Request;

public class FireMonitorDataDeleteQuery
{
    public Guid Id { get; set; }
}