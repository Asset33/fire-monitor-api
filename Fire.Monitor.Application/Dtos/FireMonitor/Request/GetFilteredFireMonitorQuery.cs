﻿namespace Fire.Monitor.Application.Dtos.FireMonitor.Request;

public class GetFilteredFireMonitorQuery
{
    public Guid? Id { get; set; }
    public string? Prob { get; set; }
    public DateTime? Date { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}