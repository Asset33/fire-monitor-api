﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast.Request;

public class GetByIdQuery
{
    public Guid Id { get; set; }
}