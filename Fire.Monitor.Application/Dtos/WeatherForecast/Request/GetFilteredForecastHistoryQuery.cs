﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast.Request;

public class GetFilteredForecastHistoryQuery
{
    public Guid? Id { get; set; }
    public string? Latitude { get; set; }
    public string? Longitude { get; set; }
    public DateTime? Date { get; set; }
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}