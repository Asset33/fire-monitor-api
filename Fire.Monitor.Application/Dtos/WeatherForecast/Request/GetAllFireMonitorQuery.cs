﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast.Request;

public class GetAllFireMonitorQuery
{
    public int PageNumber { get; set; }
    public int PageSize { get; set; }
}