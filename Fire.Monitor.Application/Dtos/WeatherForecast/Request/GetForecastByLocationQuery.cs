﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast.Request;

public class GetForecastByLocationQuery
{
    public string? Latitude { get; set; }
    public string? Longitude { get; set; }
}