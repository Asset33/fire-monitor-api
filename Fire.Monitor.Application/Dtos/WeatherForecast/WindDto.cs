﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast;

public class WindDto
{
    public decimal Speed { get; set; }
    public int Deg { get; set; }
    public decimal Gust { get; set; }
}