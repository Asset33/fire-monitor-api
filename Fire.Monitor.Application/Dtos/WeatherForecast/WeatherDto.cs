﻿using System.Text.Json.Serialization;

namespace Fire.Monitor.Application.Dtos.WeatherForecast;

public class WeatherDto
{
    public TemperatureDto? Main { get; set; }
    public SystemInfoDto? Sys { get; set; }

    public WindDto? Wind { get; set; }

    public LocationDto? Coord { get; set; }
    [JsonPropertyName("name")]
    public string? City { get; set; }
    [JsonPropertyName("dt")]
    public ulong Date { get; set; }
}