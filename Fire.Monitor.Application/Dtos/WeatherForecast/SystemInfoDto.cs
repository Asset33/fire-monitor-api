﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast;

public class SystemInfoDto
{
    public string? Country { get; set; }
}