﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast;

public class LocationDto
{
    public decimal Lat { get; set; }
    public decimal Lon { get; set; }
}