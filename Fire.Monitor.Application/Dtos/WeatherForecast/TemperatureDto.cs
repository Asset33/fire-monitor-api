﻿namespace Fire.Monitor.Application.Dtos.WeatherForecast;

public class TemperatureDto
{
    public decimal Temp { get; set; }
    public int Pressure { get; set; }
    public decimal Humidity { get; set; }
}