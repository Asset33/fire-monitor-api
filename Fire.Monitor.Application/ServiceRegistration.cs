using System.Reflection;
using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Command;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Command.Impl;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Query;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Query.Impl;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Command;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Command.Impl;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Query;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Query.Impl;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fire.Monitor.Application;

public static class ServiceRegistration
{
    public static void AddApplicationLayer(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddAutoMapper(Assembly.GetExecutingAssembly());
        services.AddScoped<IForecastQueryLogic,ForecastQueryLogic>();
        services.AddScoped<IForecastCommandLogic, ForecastCommandLogic>();
        services.AddScoped<IFireMonitorQueryLogic, FireMonitorQueryLogic>();
        services.AddScoped<IFireMonitorCommandLogic, FireMonitorCommandLogic>();
    }
}