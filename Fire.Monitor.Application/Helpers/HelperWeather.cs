﻿using System.Text.Json;
using Fire.Monitor.Application.Dtos.WeatherForecast;

namespace Fire.Monitor.Application.Helpers
{
    public static class HelperWeather
    {
        public static async Task<WeatherDto?> GetWeatherForecastAsync(string? lat, string? lon, string appid)
        {
            HttpClient client = new HttpClient();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            var response =
                await client.GetAsync(
                    $"https://api.openweathermap.org/data/2.5/weather?units=metric&lat={lat}&lon={lon}&appid={appid}&lang=ru");
            var responseString = await response.Content.ReadAsStringAsync();
            WeatherDto? weather = JsonSerializer.Deserialize<WeatherDto>(responseString, options);
            return weather;
        }
    }
}