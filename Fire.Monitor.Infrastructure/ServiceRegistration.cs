﻿using Fire.Monitor.Application.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Fire.Monitor.Infrastructure;

public static class ServiceRegistration
{
    public static void AddInfrastructureLayer(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<ApplicationDbContext>(options =>
        {
            options.UseNpgsql(
                configuration.GetConnectionString("FireMonitorDataConnection"),
                b => b.MigrationsAssembly(typeof(ApplicationDbContext).Assembly.FullName));
            options.EnableSensitiveDataLogging();
        });

        services.AddTransient<IApplicationDbContext, ApplicationDbContext>();
        //services.AddScoped<IApplicationDbContext>(provider => provider.GetService<IApplicationDbContext>() ?? throw new InvalidOperationException());
    }
}