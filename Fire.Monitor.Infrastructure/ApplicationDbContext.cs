﻿using Fire.Monitor.Application.Interfaces;
using Fire.Monitor.Domain.Entities.FireMonitor;
using Fire.Monitor.Domain.Entities.WeatherForecast;
using Microsoft.EntityFrameworkCore;

namespace Fire.Monitor.Infrastructure;

public class ApplicationDbContext : DbContext, IApplicationDbContext
{
    public DbSet<FireMonitorData> FireMonitorDatas { get; set; }
    public DbSet<Detection> Detections { get; set; }
    public DbSet<Weather> Weathers { get; set; }

    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
    : base(options)
    {

    }
}