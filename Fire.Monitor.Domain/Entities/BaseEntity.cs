﻿namespace Fire.Monitor.Domain.Entities;

public class BaseEntity<T>
{
    public T Id { get; set; }
    public DateTime Created { get; set; }
    public DateTime UpdateDate { get; set; }
    public bool IsDelete { get; set; } = false;
}