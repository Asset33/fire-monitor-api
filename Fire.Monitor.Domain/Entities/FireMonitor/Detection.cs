﻿using System.Text.Json.Serialization;

namespace Fire.Monitor.Domain.Entities.FireMonitor;

public class Detection : BaseEntity<Guid>
{
    [JsonPropertyName("prob")]
    public decimal Prob { get; set; }
        
    [JsonPropertyName("up_lat")]
    public decimal UpLat { get; set; }

    [JsonPropertyName("left_lon")]
    public decimal LeftLon { get; set; }
        
    [JsonPropertyName("down_lat")]
    public decimal Downlat { get; set; }
        
    [JsonPropertyName("right_lon")]
    public decimal RightLon { get; set; }
}