﻿namespace Fire.Monitor.Domain.Entities.FireMonitor;

public class FireMonitorData : BaseEntity<Guid>
{
    public ICollection<Detection>? Detections { get; set; }
}