﻿namespace Fire.Monitor.Domain.Entities.WeatherForecast;

public class Weather : BaseEntity<Guid>
{
    public Temperature? Main { get; set; }
    public SystemInfo? Sys { get; set; }
    
    public Wind? Wind { get; set; }
    
    public Location Coord { get; set; }
    public string City { get; set; }

    public ulong Date { get; set; }
}
