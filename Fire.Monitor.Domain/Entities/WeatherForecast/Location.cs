﻿namespace Fire.Monitor.Domain.Entities.WeatherForecast;

public class Location : BaseEntity<Guid>
{
    public decimal Lat { get; set; }
    public decimal Lon { get; set; }
}