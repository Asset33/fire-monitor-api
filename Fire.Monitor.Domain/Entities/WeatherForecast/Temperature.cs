﻿namespace Fire.Monitor.Domain.Entities.WeatherForecast;

public class Temperature : BaseEntity<Guid>
{
    public decimal Temp { get; set; }
    public int Pressure { get; set; }
    public decimal Humidity { get; set; }
}