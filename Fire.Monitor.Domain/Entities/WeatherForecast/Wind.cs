﻿namespace Fire.Monitor.Domain.Entities.WeatherForecast;

public class Wind : BaseEntity<Guid>
{
    public decimal Speed { get; set; }
    public int Deg { get; set; }
    public decimal Gust { get; set; }
}