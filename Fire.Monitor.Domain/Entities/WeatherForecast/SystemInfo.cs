﻿namespace Fire.Monitor.Domain.Entities.WeatherForecast;

public class SystemInfo : BaseEntity<Guid>
{
    public string? Country { get; set; }
}