﻿namespace Fire.Monitor.Domain.Primitives;

public enum Roles
{
    SuperAdmin,
    Admin,
    Manager,
    Operator,
    WarrantyDepartmentSpecialist
}
