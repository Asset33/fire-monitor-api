﻿using Fire.Monitor.Identity;
using Fire.Monitor.Identity.Models;
using Microsoft.AspNetCore.Identity;

namespace Fire_monitor_api.Extension;

public static class SeedData
{
    public static async Task InitializeBaseUser(this IHost host)
    {
        using (var scope = host.Services.CreateScope())
        {
            using var userManager = scope.ServiceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            using var roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            using var context = scope.ServiceProvider.GetRequiredService<AuthContext>();
            
                
            await SeedAsync(roleManager);
        }

    }
        
    public static async Task SeedAsync(RoleManager<IdentityRole> roleManager)
    {
        var roles = new string[]
        {
            //Roles.SuperAdmin
        };

        foreach (var role in roles)
        {
            await AddRole(roleManager, role);
        }
    }

    private static async Task AddRole(RoleManager<IdentityRole> roleManager, string role)
    {
        var newRole = await roleManager.FindByNameAsync(role);
        if (newRole == null)
        {
            await roleManager.CreateAsync(new IdentityRole(role));
        }
    }
        
}