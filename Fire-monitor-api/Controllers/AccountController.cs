﻿using Fire.Monitor.Identity;
using Fire.Monitor.Identity.Dtos;
using Fire.Monitor.Identity.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Fire_monitor_api.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class AccountController : Controller
{
    
    private readonly UserManager<ApplicationUser> _userManager;
    private readonly SignInManager<ApplicationUser> _signInManager;
    private readonly AuthContext _context;
    
    public AccountController(UserManager<ApplicationUser> userManager, SignInManager<ApplicationUser> signInManager, AuthContext context)
    {
        _userManager = userManager;
        _signInManager = signInManager;
        _context = context;
    }
    
    [HttpPost]
    public async Task<IActionResult> Login([FromBody] LoginDto request)
    {
        if (request != null)
        {
            var user = await _context.Users
                .Where(u => u.Email == request.Email)
                .AsNoTracking()
                .FirstOrDefaultAsync();
            var result = await _userManager.CheckPasswordAsync(user,request.Password);
            return Ok(result);
        }
        return Ok("error");
    }

    [HttpPost]
    public async Task<IActionResult> Register([FromBody] RegisterDto request)
    {
        var user = new ApplicationUser()
        {
            FirstName = request.FirstName,
            LastName = request.LastName,
            Email = request.Email,
            UserName = request.LastName
        };
        await _userManager.CreateAsync(user, request.Password);
        return Ok(user);
    }
}