﻿using Fire.Monitor.Application.Dtos.FireMonitor;
using Fire.Monitor.Application.Dtos.FireMonitor.Request;
using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Command;
using Fire.Monitor.Application.Logics.FireMonitorLogic.Query;
using Fire.Monitor.Application.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Fire_monitor_api.Controllers;

public class FireMonitorDataController : BaseController<IFireMonitorQueryLogic>
{

    private readonly IFireMonitorCommandLogic _command;
    public FireMonitorDataController(IFireMonitorQueryLogic service, IFireMonitorCommandLogic command) : base(service)
    {
        _command = command;
    }
    
    /// <summary>
    /// Получить все записи
    /// </summary>
    /// <response code="200">Successful operation</response>
    [HttpGet] [ProducesResponseType(typeof(PagedResponse<ICollection<FireMonitorDataDto>>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetAll([FromQuery]GetAllFireMonitorQuery request)
    {
        return Ok(await Service.GetAll(request));
    }

    /// <summary>
    /// Получить записи по фильтру
    /// </summary>
    /// <response code="200">Successful operation</response>
    [HttpGet] [ProducesResponseType(typeof(PagedResponse<ICollection<FireMonitorDataDto>>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetFiltered([FromQuery]GetFilteredFireMonitorQuery request)
    {
        return Ok(await Service.GetFiltered(request));
    }

    
    /// <summary>
    /// Добавить
    /// </summary>
    /// <response code="200">Successful operation</response>
    /// <response code="404">Not found</response>
    [ProducesResponseType(typeof(Response<int>),StatusCodes.Status200OK)]
    [HttpPost]
    public async Task<IActionResult> Add([FromBody]FireMonitorDataDto dto)
    {
        return Ok(await _command.Add(dto));
    }

    /// <summary>
    /// Обновить
    /// </summary>
    /// <response code="200">Successful operation</response>
    /// <response code="404">Not found</response>
    [ProducesResponseType(typeof(Response<int>),StatusCodes.Status200OK)]
    [HttpPut]
    public async Task<IActionResult> Update([FromBody]Guid id)
    {
        return Ok(await _command.Update(id));
    }
    
    /// <summary>
    /// Удалить
    /// </summary>
    /// <response code="200">Successful operation</response>
    /// <response code="404">Not found</response>
    [ProducesResponseType(typeof(Response<bool>),StatusCodes.Status200OK)]
    [HttpDelete]
    public async Task<IActionResult> Delete([FromBody]Guid id)
    {
        return Ok(await _command.Update(id));
    }
    
}
    