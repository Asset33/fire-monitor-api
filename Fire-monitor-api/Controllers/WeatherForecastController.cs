﻿using Fire.Monitor.Application.Dtos.WeatherForecast;
using Fire.Monitor.Application.Dtos.WeatherForecast.Request;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Command;
using Fire.Monitor.Application.Logics.WeatherForecastLogic.Query;
using Fire.Monitor.Application.Responses;
using Microsoft.AspNetCore.Mvc;

namespace Fire_monitor_api.Controllers;

public class WeatherForecastController : BaseController<IForecastQueryLogic>
{
    private readonly IForecastCommandLogic _command;
    public WeatherForecastController(IForecastQueryLogic service, IForecastCommandLogic command)
        : base(service)
    {
        _command = command;
    }
    
    /// <summary>
    /// Получить данные текушего прогноза погоды по координата без сохранения в БД 
    /// </summary>
    /// <response code="200">Successful operation</response>
    [ProducesResponseType(typeof(Response<WeatherDto>),StatusCodes.Status200OK)]
    [ProducesResponseType(typeof(ErrorDetails),StatusCodes.Status404NotFound)]
    [HttpGet]
    public async Task<IActionResult> GetCurrentForecastByLocation([FromQuery]GetForecastByLocationQuery request)
    {
        return Ok(await Service.GetForecastByLocation(request));
    }

    /// <summary>
    /// Получить исторические данные по прогнозу погоды из сохраненого в бд
    /// </summary>
    /// <response code="200">Successful operation</response>
    [HttpGet] [ProducesResponseType(typeof(PagedResponse<ICollection<WeatherDto>>), StatusCodes.Status200OK)]
    public async Task<IActionResult> GetFilteredForecastHistory([FromQuery] GetFilteredForecastHistoryQuery request)
    {
        return Ok (await Service.GetFilteredForecastHistory(request));
    }
    
    /// <summary>
    /// Сохранить данные по погоде в бд и получить текуший прогноз по координатам
    /// </summary>
    /// <response code="200">Successful operation</response>
    [ProducesResponseType(typeof(Response<WeatherDto>),StatusCodes.Status200OK)]
    [HttpPost]
    public async Task<IActionResult> GetAndSaveCurrentForecastByLocation([FromQuery] GetForecastByLocationQuery request)
    {
        return Ok(await _command.GetAndSaveCurrentForecastByLocation(request));
    }

    /// <summary>
    /// Удаление записей данных прогнозов погоды
    /// </summary>
    /// <response code="200">Successful operation</response>
    /// <response code="404">Not found</response>
    [ProducesResponseType(typeof(Response<int>),StatusCodes.Status200OK)]
    [HttpDelete]
    public async Task<IActionResult> Delete([FromQuery] GetByIdQuery request)
    {
        return Ok(await _command.Delete(request));
    }
}