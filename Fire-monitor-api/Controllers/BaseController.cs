﻿using Microsoft.AspNetCore.Mvc;

namespace Fire_monitor_api.Controllers;

[Route("api/[controller]/[action]")]
[ApiController]
public class BaseController<TService> : ControllerBase
{
    protected readonly TService? Service;

    public BaseController(TService service)
    {
        Service = service;
    }

    public BaseController()
    {
        
    }
}